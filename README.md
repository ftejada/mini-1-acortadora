# Mini-1-Acortadora 

Minipráctica 1: Web acortadora de URLs

Esta es una de las miniprácticas que se realizarán en la asignatura, 

Esta práctica tendrá como objetivo la creación de una aplicación web simple para acortar URLs. Los usuarios de la aplicación podrán especificar URLs, y la aplicación generará para ellos un recurso aleatorio que, a partir de ese momento, redireccionará a la URL correspondiente. La aplicación podrá realizarse según el esquema de clases explicado en clase (usando, si se quiere, el módulo webapp.py), o de cualquier otra forma.

